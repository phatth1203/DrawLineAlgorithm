#include "DrawPolygon.h"
#include <iostream>
#define PI 3.14159265359
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = PI/2;
	for (int i = 0; i < 3; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 3;
	}

	for (int i = 0; i < 3; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = PI/4;

	for (int i = 0; i < 4; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 4;
	}

	for (int i = 0; i < 4; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4],ren);
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;

	for (int i = 0; i < 5; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}

	for (int i = 0; i < 5; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = PI / 2;

	for (int i = 0; i < 6; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 6;
	}

	for (int i = 0; i < 6; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;

	for (int i = 0; i < 5; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}

	for (int i = 0; i < 5; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	int xp[5], yp[5];
	float phi = PI / 2;
	float r = R*sin(PI / 10) / sin(7 * PI / 10);
	for (int i = 0; i < 5; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		xp[i] = xc + int(r*cos(phi + PI / 5) + 0.5);
		yp[i] = yc - int(r*sin(phi + PI / 5) + 0.5);
		phi += 2 * PI / 5;
	}

	for (int i = 0; i < 5; ++i) 
	{
		Midpoint_Line(x[i], y[i], xp[i], yp[i], ren);
		Midpoint_Line(xp[i], yp[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
		
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	float phi = PI / 2;

	for (int i = 0; i < 8; ++i)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 8;
	}

	for (int i = 0; i < 8; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 3) % 8], y[(i + 3) % 8], ren);
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5];
	
	for (int i = 0; i < 5; ++i)
	{
		x[i] = xc + int(R*cos(startAngle) + 0.5);
		y[i] = yc - int(R*sin(startAngle) + 0.5);
		startAngle += 2 * PI / 5;
	}

	for (int i = 0; i < 5; ++i)
		Midpoint_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	float _r = r;
	float startAngle = PI / 2;
	while (_r > 1)
	{
		DrawStarAngle(xc, yc, int(_r + 0.5), startAngle, ren);
		startAngle += PI;
		_r*=sin(PI / 10) / sin(7 * PI / 10);
	}
}
