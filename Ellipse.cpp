#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y, p;
	int a2 = a*a;
	int b2 = b*b;
	// Area 1
	x = a, y = 0;
	p = -2 * a*b2 + b2 + 2 * a2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) >= a2*a2)
	{
		if (p <= 0)
		{
			p += 4 * a2*y + 6 * a2;
		}
		else
		{
			p += 4 * a2*y - 4 * b2*x + (4 * b2 + 6 * a2);
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = 0, y = b;
	p = -2 * b*a2 + a2 + 2 * b2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) <= a2*a2)
	{
		if (p <= 0)
		{
			p += 4 * b2*x + 6 * b2;
		}
		else
		{
			p += 4 * b2*x - 4 * a2*y + (4 * a2 + 6 * b2);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int a2 = a*a, b2 = b*b;
	int x, y, p;
	// Area 1
	 x = a, y = 0;
	 p = a2 - b2*a + (float)b2 / 4;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) >= a2*a2)
	{
		if (p <= 0)
		{
			p += 2 * a2*y + 3 * a2;
		}
		else
		{
			p += 2 * a2*y - 2 * b2*x + 2 * b2 + 3 * a2;
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = 0, y = b;
	p = b2 - a2*b + (float)a2 / 4;
	Draw4Points(xc, yc, x, y, ren);	
	while (x*x*(a2 + b2) <= a2*a2)
	{
		if(p<=0)
		{
			p += 2 * b2*x + 3 * b2;
		}
		else
		{
			p += 2 * b2*x - 2 * a2*y + 2 * a2 + 3 * b2;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
}
