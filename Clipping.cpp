#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if (c1 != 0 && c2 != 0 && c1&c2 != 0)
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	// dy = 0 OR dx = 0
	if (dx || dy)
	{
		//dx = 0
		if (dx == 0)
		{
			if (P1.y > P2.y)
			{
				P1.y = (P1.y > r.Bottom ? r.Bottom : P1.y);
				P2.y = (P2.y < r.Top ? r.Top : P2.y);
			}
			//P2.y > P1.y
			else
			{
				P2.y = (P2.y > r.Bottom ? r.Bottom : P2.y);
				P1.y = (P1.y < r.Top ? r.Top : P1.y);
			}
		}
		//dy = 0
		else 
		{
			if (P1.x > P2.x)
			{
				P1.x = (P1.x > r.Right ? r.Right : P1.x);
				P2.x = (P2.x < r.Left ? r.Left : P2.x);
			}
			//P2.x > P1.x
			else
			{
				P2.x = (P2.x > r.Right ? r.Right : P2.x);
				P1.x = (P1.x < r.Left ? r.Left : P1.x);
			}
		}
		Q1 = P1;
		Q2 = P2;
		return 1;
	}

	CODE C1 = Encode(r, P1);
	CODE C2 = Encode(r, P2);
	while (CheckCase(C1, C2) == 3)
	{
		ClippingCohenSutherland(r, P1, P2);
		 C1 = Encode(r, P1);
		 C2 = Encode(r, P2);
	}
	if (CheckCase(C1, C2) == 1)
	{
		Q1 = P1;
		Q2 = P2;
		return 1;
	}
	return 0;
}
// xmin = left, ymin = top
void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	double x, y;
	CODE C1 = Encode(r, P1);
	CODE C2 = Encode(r, P2);
	CODE C0 = (C1 != 0 ? C1 : C2);
	if (C0 & TOP)
	{
		x = P1.x + dx*(r.Top - P1.y) / dy;
		y = r.Top;
	}
	else if (C0 & BOTTOM)
	{
		x = P1.x + dx*(r.Bottom - P1.y) / dy;
		y = r.Bottom;
	}
	else if (C0 & RIGHT)
	{
		y = P1.y + dy*(r.Right - P1.x) / dx;
		x = r.Right;
	}
	else if (C0 & LEFT)
	{
		y = P1.y + dy*(r.Left - P1.x) / dx;
		x = r.Left;
	}
	if (C0 == C1)
	{
		P1.x = x;
		P1.y = y;
	}
	else 
	{
		P2.x = x;
		P2.y = y;
	}

}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1 = 0, t2 = 1, p[4], q[4];
	int dx = P2.x - P1.x;
	int dy = P2.y - P1.y;
	p[0] = -dx;
	p[1] = dx;
	p[2] = -dy;
	p[3] = dy;

	q[0] = P1.x - r.Left;
	q[1] = r.Right - P1.x;
	q[2] = P1.y - r.Top;
	q[3] = r.Bottom - P1.y;

	for (int i = 0; i < 4; ++i)
	{
		SolveNonLinearEquation(p[i], q[i], t1, t2);
	}
	
	if (t1 < t2)
	{
		 Q1.x = P1.x + t1*dx;
		 Q2.x = P1.x + t2*dx;
		 Q1.y = P1.y + t1*dy;
		 Q2.y = P1.y + t2*dy;
		 return 1;
	}
	return 0;
}
