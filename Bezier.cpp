#include "Bezier.h"
#include <iostream>
#include <math.h>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	for (double t = 0; t <= 1; t += 0.0005)
	{
		int xt = pow((1 - t), 2)*p1.x + 2 * (1 - t)*t*p2.x + t*t*p3.x;
		int yt = pow((1 - t), 2)*p1.y + 2 * (1 - t)*t*p2.y + t*t*p3.y;
		SDL_RenderDrawPoint(ren, xt, yt);
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	for (double t = 0; t <= 1; t += 0.0005)
	{
		int xt = pow((1 - t), 3)*p1.x + 3 * pow((1 - t), 2)*t*p2.x + 3 * (1 - t)*t*p3.x + t*t*t*p4.x;
		int yt = pow((1 - t), 3)*p1.y + 3 * pow((1 - t), 2)*t*p2.y + 3 * (1 - t)*t*p3.y + t*t*t*p4.y;
		SDL_RenderDrawPoint(ren, xt, yt);
	}
}

